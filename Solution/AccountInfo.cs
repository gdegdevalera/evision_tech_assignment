﻿using System.Threading.Tasks;

namespace Solution
{
    public class AccountInfo
    {
        private readonly int _accountId;
        private readonly IAccountService _accountService;

        private readonly object _guard = new object();

        // 'volatile' prevents harmful compiler optimizations
        private volatile Task _refreshTask;

        public AccountInfo(int accountId, IAccountService accountService)
        {
            _accountId = accountId;
            _accountService = accountService;
        }

        public double Amount { get; private set; }

        public Task RefreshAmount()
        {
            // If the task is running, return this one
            if (_refreshTask?.IsCompleted == false)
                return _refreshTask;

            // If task is absent or not running, only single thread can create it
            lock (_guard)
            {
                // Double checked locking (https://en.wikipedia.org/wiki/Double-checked_locking)
                if (_refreshTask?.IsCompleted == false)
                    return _refreshTask;

                return _refreshTask = _accountService.GetAccountAmount(_accountId).ContinueWith(x => Amount = x.Result);
            }
        }
    }
}