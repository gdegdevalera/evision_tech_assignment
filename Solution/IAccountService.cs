﻿using System.Threading.Tasks;

namespace Solution
{
    public interface IAccountService
    {
        Task<double> GetAccountAmount(int accountId);
    }
}