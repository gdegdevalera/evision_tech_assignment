﻿using System;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using Moq;
using NUnit.Framework;
using Solution;

namespace Tests
{
    [TestFixture]
    public class AccountInfoTests
    {
        [SetUp]
        public void SetUp()
        {
            _accountServiceMock = new Mock<IAccountService>();
        }

        private const int AccountId = 123;
        private const double ExpectedAmount = 111d;

        private Mock<IAccountService> _accountServiceMock;

        [Test]
        public async Task TestRefreshAmount()
        {
            // arrange
            const double expectedAmount1 = ExpectedAmount;
            const double expectedAmount2 = ExpectedAmount + 1;

            var accountInfo = new AccountInfo(AccountId, _accountServiceMock.Object);
            _accountServiceMock
                .SetupSequence(x => x.GetAccountAmount(AccountId))
                .Returns(Task.FromResult(expectedAmount1))
                .Returns(Task.FromResult(expectedAmount2));

            // act
            await accountInfo.RefreshAmount();
            var actualAmount1 = accountInfo.Amount;

            await accountInfo.RefreshAmount();
            var actualAmount2 = accountInfo.Amount;

            // assert
            actualAmount1.Should().Be(expectedAmount1);
            actualAmount2.Should().Be(expectedAmount2);
            _accountServiceMock.Verify(x => x.GetAccountAmount(AccountId), Times.Exactly(2));
        }

        [Test]
        public async Task TestRefreshAmountConcurrently()
        {
            // arrange
            var accountInfo = new AccountInfo(AccountId, _accountServiceMock.Object);
            _accountServiceMock
                .Setup(x => x.GetAccountAmount(AccountId))
                .Returns(Task.Delay(1000).ContinueWith(x => ExpectedAmount));

            // act
            var tasks = Enumerable.Range(0, 10).Select(_ => accountInfo.RefreshAmount());
            await Task.WhenAll(tasks);

            // assert
            accountInfo.Amount.Should().Be(ExpectedAmount);
            _accountServiceMock.Verify(x => x.GetAccountAmount(AccountId), Times.Once);
        }

        [Test]
        public async Task TestUnreliableRefreshAmount()
        {
            // arrange
            var accountInfo = new AccountInfo(AccountId, _accountServiceMock.Object);
            _accountServiceMock
                .SetupSequence(x => x.GetAccountAmount(AccountId))
                .Returns(Task.FromException<double>(new Exception("test")))
                .Returns(Task.FromResult(ExpectedAmount));

            // act
            Func<Task> act = () => accountInfo.RefreshAmount();
            act.Should().Throw<Exception>();

            await accountInfo.RefreshAmount();

            // assert
            accountInfo.Amount.Should().Be(ExpectedAmount);
            _accountServiceMock.Verify(x => x.GetAccountAmount(AccountId), Times.Exactly(2));
        }
    }
}