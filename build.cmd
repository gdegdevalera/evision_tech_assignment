dotnet clean
IF ERRORLEVEL 1 GOTO exit
dotnet build 
IF ERRORLEVEL 1 GOTO exit
dotnet test Tests
IF ERRORLEVEL 1 GOTO exit
dotnet pack Solution /p:NuspecFile=Solution.nuspec -c Release
:exit

